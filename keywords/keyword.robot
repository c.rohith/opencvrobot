*** Settings ***
Library         ScreenCapLibrary
Variables       C:/Users/ROHITH/PycharmProjects/opencvrobot/variables/varaible.yaml


*** Keywords ***
screenshot
    take screenshot     ${name}     ${format}   ${quality}  ${width}    ${delay}    ${monitor}

start_gif
    start gif recording   ${gname}   ${size}   ${boolean}    ${width}    ${monitor}      ${boolean}
    sleep    10s

stop_gif
    stop gif recording

video_rec
    start video recording    ${alias}   ${vname}    ${fps}   ${size}   ${boolean}    ${width}   ${monitor}
    sleep    10s

stop_rec
    stop video recording

mul_scs
    take multiple screenshots     ${name}     ${format}   ${quality}  ${sc_no}    ${delay}    ${monitor}

